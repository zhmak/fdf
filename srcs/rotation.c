/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotation.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/25 23:38:43 by eloren-l          #+#    #+#             */
/*   Updated: 2018/12/28 20:38:31 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		rotate_x(double x, t_point *point)
{
	double rad;
	double y_local;
	double z_local;

	y_local = point->y_r;
	z_local = point->z_r;
	rad = M_PI / 180 * x;
	point->y_r = y_local * cos(rad) - z_local * sin(rad);
	point->z_r = y_local * sin(rad) + z_local * cos(rad);
}

static void		rotate_y(double y, t_point *point)
{
	double rad;
	double x_local;
	double z_local;

	x_local = point->x_r;
	z_local = point->z_r;
	rad = M_PI / 180 * y;
	point->x_r = x_local * cos(rad) + z_local * sin(rad);
	point->z_r = -x_local * sin(rad) + z_local * cos(rad);
}

static void		rotate_z(double z, t_point *point)
{
	double rad;
	double x_local;
	double y_local;

	x_local = point->x_r;
	y_local = point->y_r;
	rad = M_PI / 180 * z;
	point->x_r = x_local * cos(rad) - y_local * sin(rad);
	point->y_r = x_local * sin(rad) + y_local * cos(rad);
}

static void		check_angles(t_mlx *mlx)
{
	if (mlx->z_a / 360 == 1)
		mlx->z_a = mlx->z_a - 360;
	if (mlx->z_a / 360 == -1)
		mlx->z_a = mlx->z_a + 360;
	if (mlx->y_a / 360 == 1)
		mlx->y_a = mlx->y_a - 360;
	if (mlx->y_a / 360 == -1)
		mlx->y_a = mlx->y_a + 360;
	if (mlx->x_a / 360 == 1)
		mlx->x_a = mlx->x_a - 360;
	if (mlx->x_a / 360 == -1)
		mlx->x_a = mlx->x_a + 360;
}

int				rotate_points(t_mlx *mlx)
{
	int			i;
	int			j;
	t_point		***points;

	points = (t_point ***)mlx->points;
	j = 0;
	check_angles(mlx);
	while (points[j] != NULL)
	{
		i = -1;
		while (points[j][++i] != NULL)
		{
			points[j][i]->x_r = points[j][i]->x;
			points[j][i]->y_r = points[j][i]->y;
			points[j][i]->z_r = points[j][i]->z / mlx->scale_z;
			if (mlx->z_a)
				rotate_z(mlx->z_a, points[j][i]);
			if (mlx->y_a)
				rotate_y(mlx->y_a, points[j][i]);
			if (mlx->x_a)
				rotate_x(mlx->x_a, points[j][i]);
		}
		j++;
	}
	return (0);
}
