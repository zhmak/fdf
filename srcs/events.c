/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/27 16:41:12 by eloren-l          #+#    #+#             */
/*   Updated: 2018/12/31 16:05:29 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	check_scale_offset(int key, t_mlx *mlx)
{
	if (key == 123)
		mlx->offset_x -= 10;
	else if (key == 124)
		mlx->offset_x += 10;
	else if (key == 126)
		mlx->offset_y -= 10;
	else if (key == 125)
		mlx->offset_y += 10;
	else if (key == 75)
		mlx->scale -= 1;
	else if (key == 67)
		mlx->scale += 1;
	else
		return (0);
	return (1);
}

static int	check_rotation(int key, t_mlx *mlx)
{
	if (key == 83)
		mlx->z_a -= mlx->rotation_angle;
	else if (key == 85)
		mlx->z_a += mlx->rotation_angle;
	else if (key == 86)
		mlx->y_a -= mlx->rotation_angle;
	else if (key == 88)
		mlx->y_a += mlx->rotation_angle;
	else if (key == 89)
		mlx->x_a -= mlx->rotation_angle;
	else if (key == 92)
		mlx->x_a += mlx->rotation_angle;
	else if (key == 91)
		mlx->x_a = 0;
	else if (key == 87)
		mlx->y_a = 0;
	else if (key == 84)
		mlx->z_a = 0;
	else
		return (0);
	return (1);
}

static int	check_z(int key, t_mlx *mlx)
{
	if (key == 121)
	{
		mlx->scale_z += 1;
		if (mlx->scale_z == 11)
			mlx->scale_z--;
	}
	else if (key == 116)
	{
		mlx->scale_z -= 1;
		if (mlx->scale_z == 0)
			mlx->scale_z++;
	}
	else
		return (0);
	return (1);
}

static int	check_angle(int key, t_mlx *mlx)
{
	if (key == 78)
	{
		if (mlx->rotation_angle > 5)
			mlx->rotation_angle -= 5;
		else if (mlx->rotation_angle == 1)
			;
		else if (mlx->rotation_angle <= 5)
			mlx->rotation_angle -= 1;
		return (1);
	}
	if (key == 69)
	{
		if (mlx->rotation_angle < 5)
			mlx->rotation_angle += 1;
		else if (mlx->rotation_angle < 90)
			mlx->rotation_angle += 5;
		return (1);
	}
	return (0);
}

int			check_key(int key, t_mlx *mlx)
{
	int flag;

	flag = 0;
	if (key == 53)
		exit(0);
	else if (check_rotation(key, mlx) || check_scale_offset(key, mlx) ||
		check_z(key, mlx))
		flag = 1;
	else if (check_angle(key, mlx))
		flag = 2;
	if (flag == 0)
		return (0);
	if (flag == 1)
	{
		mlx_destroy_image(mlx->mlx_ptr, mlx->img.img_ptr);
		mlx->img.img_ptr = mlx_new_image(mlx->mlx_ptr, 1400, 1100);
		create_image(mlx);
	}
	mlx_clear_window(mlx->mlx_ptr, mlx->win_ptr);
	window_strings(mlx);
	mlx_put_image_to_window(mlx->mlx_ptr, mlx->win_ptr,
						mlx->img.img_ptr, 0, 100);
	return (1);
}
