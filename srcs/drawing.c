/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/27 16:44:46 by eloren-l          #+#    #+#             */
/*   Updated: 2018/12/30 18:09:08 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		line_to_image(t_mlx *mlx, t_line line)
{
	int		steps;
	double	dx;
	double	dy;
	double	x_inc;
	double	y_inc;

	dx = line.x_r1 - line.x_r0;
	dy = line.y_r1 - line.y_r0;
	if (fabs(dx) > fabs(dy))
		steps = fabs(dx);
	else
		steps = fabs(dy);
	x_inc = dx / (double)steps;
	y_inc = dy / (double)steps;
	while (steps)
	{
		if (line.x_r0 > 0 && line.x_r0 < 1400 &&
			line.y_r0 > 0 && line.y_r0 < 1100)
			mlx->img.image[(int)line.x_r0 + (int)line.y_r0 * 1400] = 0x00FF00;
		line.x_r0 += x_inc;
		line.y_r0 += y_inc;
		steps--;
	}
}

static void		adjust_points(t_point ***points, t_mlx *mlx)
{
	int i;
	int j;

	j = 0;
	rotate_points(mlx);
	while (points[j] != NULL)
	{
		i = 0;
		while (points[j][i] != NULL)
		{
			points[j][i]->x_r = (points[j][i]->x_r * mlx->scale)\
			+ mlx->offset_x;
			points[j][i]->y_r = (points[j][i]->y_r * mlx->scale)\
			+ mlx->offset_y;
			i++;
		}
		j++;
	}
}

static t_line	points_side(t_point ***points, int j, int i)
{
	t_line	line;

	line.x_r0 = points[j][i]->x_r;
	line.y_r0 = points[j][i]->y_r;
	line.x_r1 = points[j][i + 1]->x_r;
	line.y_r1 = points[j][i + 1]->y_r;
	return (line);
}

static t_line	points_down(t_point ***points, int j, int i)
{
	t_line	line;

	line.x_r0 = points[j][i]->x_r;
	line.y_r0 = points[j][i]->y_r;
	line.x_r1 = points[j + 1][i]->x_r;
	line.y_r1 = points[j + 1][i]->y_r;
	return (line);
}

void			create_image(t_mlx *mlx)
{
	int		i;
	int		j;
	t_point	***points;

	points = (t_point ***)mlx->points;
	adjust_points(points, mlx);
	j = 0;
	while (points[j] != NULL)
	{
		i = 0;
		while (points[j][i] != NULL)
		{
			if (i < mlx->colms - 1)
				line_to_image(mlx, points_side(points, j, i));
			if (j < mlx->rows - 1)
				line_to_image(mlx, points_down(points, j, i));
			i++;
		}
		j++;
	}
}
