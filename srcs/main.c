/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/24 18:14:17 by eloren-l          #+#    #+#             */
/*   Updated: 2019/01/01 15:03:31 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <stdio.h>

static void		window_angle(t_mlx *mlx)
{
	char *string;
	char *buff;

	string = ft_strjoin("CURRENT ROTATIONS (DEGREES):   \
					X: ", ft_itoa(mlx->x_a));
	buff = string;
	string = ft_strjoin(string, " | Y: ");
	free(buff);
	buff = string;
	string = ft_strjoin(string, ft_itoa(mlx->y_a));
	free(buff);
	buff = string;
	string = ft_strjoin(string, " | Z: ");
	free(buff);
	buff = string;
	string = ft_strjoin(string, ft_itoa(mlx->z_a));
	free(buff);
	mlx_string_put(mlx->mlx_ptr, mlx->win_ptr, 10, 50, 0x0000FF00, string);
	free(string);
	string = ft_strjoin("CURRENT ROTATION ANGLE (DEGREES):   ",
					ft_itoa(mlx->rotation_angle));
	mlx_string_put(mlx->mlx_ptr, mlx->win_ptr, 10, 70, 0x0000FF00, string);
	free(string);
}

void			window_strings(t_mlx *mlx)
{
	mlx_string_put(mlx->mlx_ptr, mlx->win_ptr, 10, 10, 0x0000FF00,
				"ROTATIONS (NUMPAD):   X AXIS: 7 - 9 | ROTATE Y AXIS: 4 - 6 | \
				ROTATE Z AXIS: 1 - 3 | \
				SET RESPECTIVE AXIS TO 0: X - 8; Y - 5; Z - 2");
	mlx_string_put(mlx->mlx_ptr, mlx->win_ptr, 10, 30, 0x0000FF00,
				"SCALE Z: PAGEUP - PAGEDOWN | SCALE MAP: * - / | \
				CHANGE ROTATION ANGLE: + - | MOVE MAP: ARROWS");
	window_angle(mlx);
}

static void		initial_offset_scale(t_mlx *mlx)
{
	mlx->offset_x = 700;
	mlx->offset_y = 550;
	mlx->scale_z = 1;
	mlx->scale = 1200 / sqrt(mlx->rows * mlx->rows + mlx->colms * mlx->colms);
}

static void		error_check(int argc, char *argv)
{
	int fd;

	if (argc != 2)
	{
		ft_putstr("usage: fdf map_file\n");
		exit(0);
	}
	fd = open(argv, O_RDWR);
	if (fd < 0)
	{
		ft_putstr("Invalid file. Exiting.\n");
		exit(0);
	}
	close(fd);
}

int				main(int argc, char **argv)
{
	t_mlx	mlx;

	error_check(argc, argv[1]);
	mlx.points = read_file(argv[1], &mlx);
	initial_offset_scale(&mlx);
	mlx.x_a = 0;
	mlx.y_a = 0;
	mlx.z_a = 0;
	mlx.rotation_angle = 5;
	mlx.mlx_ptr = mlx_init();
	mlx.win_ptr = mlx_new_window(mlx.mlx_ptr, 1400, 1200, "fdf");
	mlx.img.img_ptr = mlx_new_image(mlx.mlx_ptr, 1400, 1100);
	mlx.img.image = (int *)(mlx_get_data_addr(mlx.img.img_ptr, &mlx.img.bits,
					&mlx.img.colms, &mlx.img.endian));
	create_image(&mlx);
	mlx_put_image_to_window(mlx.mlx_ptr, mlx.win_ptr, mlx.img.img_ptr, 0, 100);
	mlx_key_hook(mlx.win_ptr, check_key, &mlx);
	window_strings(&mlx);
	mlx_loop(mlx.mlx_ptr);
	return (0);
}
