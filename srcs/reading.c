/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reading.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/24 22:47:46 by eloren-l          #+#    #+#             */
/*   Updated: 2019/01/01 14:41:08 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		write_line(t_mlx *mlx, int row, int fd, t_point ***points)
{
	int		colm;
	char	*line;
	char	**coords;
	int		colm_count;

	get_next_line(fd, &line);
	coords = ft_strsplit(line, ' ');
	colm_count = count_colms(coords);
	if (colm_count != mlx->colms)
	{
		ft_putstr("Invalid map. Exiting.\n");
		exit(0);
	}
	points[row] = (t_point **)malloc(sizeof(t_point *) * (mlx->colms + 1));
	colm = 0;
	while (colm < mlx->colms)
	{
		points[row][colm] = (t_point *)malloc(sizeof(t_point));
		points[row][colm]->x = colm - mlx->colms / 2;
		points[row][colm]->y = row - mlx->rows / 2;
		points[row][colm]->z = ft_atoi(coords[colm]);
		colm++;
	}
	points[row][colm] = NULL;
	free(coords);
}

t_point			***read_file(char *map_name, t_mlx *mlx)
{
	int		fd;
	int		row;
	t_point ***points;

	mlx->rows = count_lines(map_name);
	if (mlx->rows < 2)
	{
		ft_putstr("Invalid map. Exiting.\n");
		exit(0);
	}
	mlx->colms = count_colms_start(map_name);
	points = (t_point ***)malloc(sizeof(t_point **) * (mlx->rows + 1));
	fd = open(map_name, O_RDONLY);
	row = 0;
	while (row < mlx->rows)
	{
		write_line(mlx, row, fd, points);
		row++;
	}
	points[row] = NULL;
	close(fd);
	return (points);
}
