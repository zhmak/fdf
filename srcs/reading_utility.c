/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reading_utility.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/28 22:40:09 by eloren-l          #+#    #+#             */
/*   Updated: 2018/12/28 23:34:30 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	free_coords(char **coords)
{
	int i;

	i = 0;
	while (coords[i] != NULL)
	{
		free(coords[i]);
		i++;
	}
	free(coords[i]);
	free(coords);
}

int		count_colms_start(char *map)
{
	int		i;
	int		fd;
	int		count;
	char	*line;
	char	**coords;

	fd = open(map, O_RDONLY);
	get_next_line(fd, &line);
	coords = ft_strsplit(line, ' ');
	i = 0;
	count = 0;
	while (coords[i] != NULL)
	{
		count++;
		i++;
	}
	return (count);
}

int		count_colms(char **coords)
{
	int		count;

	count = 0;
	while (coords[count] != NULL)
		count++;
	return (count);
}

int		count_lines(char *map_name)
{
	int		count_rows;
	int		check;
	int		fd;
	char	buff;

	fd = open(map_name, O_RDONLY);
	count_rows = 0;
	check = 1;
	while (check)
	{
		check = read(fd, &buff, 1);
		if (buff == '\n')
			count_rows++;
	}
	close(fd);
	return (count_rows - 1);
}
