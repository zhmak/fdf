# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/20 15:49:41 by eloren-l          #+#    #+#              #
#    Updated: 2019/01/01 15:04:15 by eloren-l         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# BASIC DIRECTORIES
SRC_DIR = srcs
INC_DIR = includes
OBJ_DIR = objs

# PATHS TO C SOURCE FILES
SRC_PATH = $(wildcard $(SRC_DIR)/*.c)

# PATHS TO OBJECTS
OBJ_PATH = $(SRC_PATH:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)

# PATH TO LIBFT

LIB_NAME = libft.a
LIB_PATH = libft
LIB = -lft

# MLX INFO

MLX = -lmlx -framework OpenGL -framework AppKit
MLX_PATH = minilibx_macos
MLX_NAME = libmlx.a

# COMPILATION RULES
CC = gcc
FLAGS = -Wall -Wextra -Werror
NAME = fdf

# DEBUG
#FLAGS += -g

.PHONY: all clean fclean re

all: $(OBJ_DIR) $(NAME)

$(NAME): $(OBJ_PATH) $(LIB_PATH)/$(LIB_NAME) $(MLX_PATH)/$(MLX_NAME)
	$(CC) $(FLAGS) $(OBJ_PATH) -o $@ -I$(INC_DIR) -I$(LIB_PATH)/$(INC_DIR) -I$(MLX_PATH) -L$(LIB_PATH) $(LIB) -L$(MLX_PATH) $(MLX) 

$(OBJ_DIR):
	mkdir -p $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(FLAGS) -o $@ -c $< -I$(INC_DIR) -I$(LIB_PATH)/$(INC_DIR) -I$(MLX_PATH)

$(LIB_PATH)/$(LIB_NAME):
	make -C $(LIB_PATH)

$(MLX_PATH)/$(MLX_NAME):
	make -C $(MLX_PATH)

clean:
	rm -rf $(OBJ_DIR)
	make -C $(LIB_PATH) clean

fclean: clean
	rm -f $(NAME)
	make -C $(LIB_PATH) fclean
	make -C $(MLX_PATH) clean

re: fclean all

