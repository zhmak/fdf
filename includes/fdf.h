/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/24 17:54:02 by eloren-l          #+#    #+#             */
/*   Updated: 2018/12/29 20:12:09 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <stdlib.h>
# include <math.h>
# include <fcntl.h>
# include <unistd.h>
# include "mlx.h"
# include "libft.h"
# include "get_next_line.h"

typedef struct	s_point
{
	int			x;
	int			y;
	int			z;
	double		x_r;
	double		y_r;
	double		z_r;
}				t_point;

typedef struct	s_line
{
	double		x_r0;
	double		y_r0;
	double		x_r1;
	double		y_r1;

}				t_line;

typedef struct	s_img
{
	int			bits;
	int			colms;
	int			endian;
	int			*image;
	void		*img_ptr;
}				t_img;

typedef struct	s_mlx
{
	int			x_a;
	int			y_a;
	int			z_a;
	int			colms;
	int			rows;
	int			scale;
	int			scale_z;
	int			offset_x;
	int			offset_y;
	int			rotation_angle;
	void		*mlx_ptr;
	void		*win_ptr;
	void		*points;
	t_img		img;
}				t_mlx;

int				rotate_points(t_mlx *mlx);
t_point			***read_file(char *map_name, t_mlx *mlx);
void			create_image(t_mlx *mlx);
int				check_key(int key, t_mlx *mlx);
void			window_strings(t_mlx *mlx);
void			free_coords(char **coords);
int				count_colms_start(char *map);
int				count_lines(char *map_name);
int				count_colms(char **coords);

#endif
